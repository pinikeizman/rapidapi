import React, {Component} from 'react';
import './App.sass';
import {FlexBox} from '../Styled/styled';
import LoginComponent from '../Login/LoginComponent';
import {Route} from "react-router-dom";
import HomeComponent from '../Home';

class App extends Component {
    render() {
        return (
            <FlexBox alignItems="center" flexDirection="column" className="App">
                <Route exact path="/" component={LoginComponent}/>
                <Route path="/home" component={HomeComponent}/>
            </FlexBox>
        );
    }
}

export default App;
