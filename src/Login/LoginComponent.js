import React, {Component} from 'react';
import {FlexBox, StyledLabel, StyledInput, StyledButton, ErrorLabel} from '../Styled/styled';
import {withRouter} from 'react-router'
import * as api from '../api';

const sha256 = require('sha256');
const _ = require('lodash');

const inputsKeys = {
    passwordKey: "password",
    usernameKey: "username"
}

class LoginComponent extends Component {

    state = {
        [inputsKeys.usernameKey]: "",
        [inputsKeys.passwordKey]: ""
    }

    login = () => {
        const user = api.getUser(this.state[inputsKeys.usernameKey]);
        if (_.get(user, inputsKeys.passwordKey) === sha256(this.state[inputsKeys.passwordKey])) {
            this.props.history.push('home');
            api.setCurrentUser(user.username);
            return;
        }
        this.setState({loginError: "Please provid valid credentials."});
    }

    handleInputOnChnage = (value, inputName) => {
        switch (inputName) {
            case inputsKeys.usernameKey:
                this.setState({[inputsKeys.usernameKey]: value});
                break;
            case inputsKeys.passwordKey:
                this.setState({[inputsKeys.passwordKey]: value});
                break;
            default:
        }
        this.setState({loginError: ""});
    }

    render() {
        return (<FlexBox flexDirection="column" alignItems="center">
            <StyledLabel style={{
                fontSize: 38
            }}>
                Login
            </StyledLabel>
            <StyledInput className="form-input"
                         value={this.state[inputsKeys.usernameKey]}
                         onChange={(e) => this.handleInputOnChnage(e.target.value, inputsKeys.usernameKey)}
                         placeholder="Username"
                         style={{marginBottom: 10}}
                         type="text"/>
            <StyledInput className="form-input"
                         value={this.state[inputsKeys.passwordKey]}
                         onChange={(e) => this.handleInputOnChnage(e.target.value, inputsKeys.passwordKey)}
                         placeholder="Password"
                         style={{marginBottom: 10}}
                         type="password"/>
            <StyledButton onClick={this.login}>
                Let me in.
            </StyledButton>
            {
                this.state.loginError ?
                    <ErrorLabel>
                        {this.state.loginError}
                    </ErrorLabel>
                    : null
            }
        </FlexBox>)
    }

}

export default withRouter(LoginComponent);
