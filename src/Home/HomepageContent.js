import React from 'react';
import {FlexBox, StyledErrorButton} from '../Styled/styled';
import Logo from '../images/hello.svg';
import styled from 'styled-components';
import * as api from '../api';
import {withRouter} from 'react-router'

const SmallStyledErrorLabel = styled(StyledErrorButton)`
  font-size:14px;
  padding:5px 10px;
  height: 25px;
  margin: 0px;
  min-width:unset;
  width:auto;
  box-shadow: 0px 0px 5px 0px black;
  top:5px;
  :active{
    transform: scale(0.90,0.90);
  }
`

class HomepageContent extends React.Component{


    onLogout = () => {
        const currentUser = api.getCurrentUser();
        currentUser.position = {...this.props.getCurrentPosition()};
        api.setUser(currentUser);
        api.setCurrentUser("");
        this.props.history.push("/")
    }

    render() {
        return (
            <FlexBox flexDirection="column" justifyContent="center" alignItems="center" className="headerContainer">
                <img className="LogoImg" alt="Awesome Logo" src={Logo}/>
                <SmallStyledErrorLabel onClick={this.onLogout}>
                    Logout
                </SmallStyledErrorLabel>
            </FlexBox>
        );
    }

}

export default withRouter(HomepageContent)
