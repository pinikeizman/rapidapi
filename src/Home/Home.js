import React, {Component} from 'react';
import './Home.sass';
import {FlexBox} from '../Styled/styled';
import styled from 'styled-components';
import UserIcon from '../images/user.png';
import Draggable from 'react-draggable'; // Both at the same time
import * as api from '../api';
import {withRouter} from 'react-router'
import HomepageContent from './HomepageContent'

const UserIconImg = styled.img`
  width:100px;
  height:100px;
  z-index:1
  box-shadow: 0px 0px 10px 0px #333
  border-radius:100%;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -o-user-select: none;
  user-select: none;
 `

const BubbleBox = styled.div`
  width: 100px
  height: 100px
  position: absolute
  background: white
  border-radius:100%
  z-index: 0
  animation: bubble 1s infinite
`
class Home extends Component {

    state = {
        currentPosition: (api.getCurrentUser() && api.getCurrentUser().position) || {x: 0, y: 0}
    };

    onStop = (e, position) => {
        this.setState({currentPosition: {x: position.x, y: position.y}, dragging: false});
        const currentUser = {...api.getCurrentUser()};
        currentUser.position = {x: position.x, y: position.y};
        api.setUser(currentUser);
    }
    onStart = (e, position) => {
        this.setState({dragging: true})
    }

    componentDidMount() {
        if (!api.getCurrentUser()) {
            this.props.history.push('/');
        }
    }


    render() {
        const user = api.getCurrentUser();
        return (
            <FlexBox
                flexDirection="column"
                justifyContent="center"
                alignSelf="normal"
                alignItems="center">
                <HomepageContent getCurrentPosition={() => this.state.currentPosition}/>
                <Draggable
                    position={this.state.currentPosition}
                    onStop={this.onStop}
                    onStart={this.onStart}>
                    <FlexBox>
                        <div>
                            <UserIconImg
                                draggable="false"
                                style={{cursor: 'pointer', position: "absolute"}}
                                src={UserIcon}/>
                            {this.state.dragging ? <BubbleBox/> : null}
                        </div>
                        <FlexBox style={{
                            position: "relative",
                            color: "rgba(255,255,255,0.7)",
                            top: 105,
                            left: -20
                        }}
                                 flexDirection="column">
                            <span>{ user && user.nickname }</span>
                            <span>{ user && user.username }</span>
                        </FlexBox>
                    </FlexBox>
                </Draggable>
            </FlexBox>
        );
    }
}

export default withRouter(Home);
