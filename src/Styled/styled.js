import styled from "styled-components";

export const FlexBox = styled.div`
                display:flex;
                flex: ${props => !props.flex ? "" : props.flex };
                flex-direction: ${props => !props.flexDirection ? "" : props.flexDirection };
                justify-content:${props => !props.justifyContent ? "" : props.justifyContent};
                align-items:${props => !props.alignItems ? "" : props.alignItems};
                align-self:${props => !props.alignSelf ? "" : props.alignSelf}
`;
export const ErrorLabel = styled.span`
    font-size: 14px;
    font-family:'Roboto';
    color: rgba(255,0,0,0.8);
    box-shadow: 0px 0px 3px 0px black;
    border-radius:5px;
    background: #ddd;
    padding: 5px 10px;
    margin-top:10px;
    margin-bottom:10px;
`;

export const StyledInput = styled.input`
    height:40px;
    color: rgba(252,253,254,0.8);
    font-size:14px;
    height:40px;
    padding: 5px 10px;
    border-radius: 4px;
    background-color:#2f2c3c;
    border: none;
    box-shadow:  rgba(0,0,0,1) 0px 0px 2px 0px inset ,rgba(255,255,255,.2) 0.5px 1px 2px 0px;
    text-shadow: rgba(0,0,0,.4) 0 1px 0;
    min-width:350px;
    text-align:${ props => !props.textAlign ? "left" : props.textAlign};
    box-sizing: border-box;
    :focus{
        outline:none;
    }
`;

export const StyledLabel = styled.label`
    color:#fcfdfe;
    height:40px;
    font-size:18px;
    font-weight: bold;
    line-height:40px;
    min-width:150px;
    margin-bottom:15px;
    text-shadow: 0px 0px 3px rgba(0,0,0,0.5);
`;

export const StyledButton = styled.button`
    cursor:pointer;
    color:#fcfdfe;
    font-size:18px;
    text-align:center;
    background: linear-gradient(#6eaedd, #517bd3);
    height:40px;
    border-radius:5px;
    text-shadow: 0px 0px 3px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.5),0px 0px 2px 0px rgba(0,0,0,0.5) inset;
    min-width:150px;
    width:100%;
    margin-bottom:15px;
    border: none;
    :focus{
        outline:none;
    }
    :active{
      transform: scale(0.95,0.95);
    }
`;
export const StyledErrorButton = styled.button`
    cursor:pointer;
    color:#fcfdfe;
    font-size:18px;
    text-align:center;
    background:rgba(255,0,0,.8);
    height:40px;
    border-radius:5px;
    text-shadow: 0px 0px 3px rgba(0,0,0,0.5);
    box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.5),0px 0px 2px 0px rgba(0,0,0,0.5) inset;
    min-width:150px;
    width:100%;
    margin-bottom:15px;
    border: none;
    :focus{
        outline:none;
    }
    :active{
      transform: scale(0.95,0.95);
    }
`;

export const StyledTextArea = StyledInput.withComponent('textarea');


export const StyledJsonArea = styled(StyledTextArea)`
    width: 100%;
    border: none;
    font-size: 24px;
    :focus{
        box-shadow: none;
        border: none;
    }
`;

export default {
    FlexBox: FlexBox,
    ErrorLabel: ErrorLabel,
    StyledInput: StyledInput,
    StyledLabel: StyledLabel,
    StyledButton: StyledButton,
    StyledTextArea: StyledTextArea,
    StyledJsonArea: StyledJsonArea


}
