# README #


### What is this repository for? ###

RapidApi frontend developer interview task.
This application is really simple.
The app accept two input at login page,
in login phase the app will try to
fetch the user from DB and match the password using sha256 signature.
After successful login the user is re-directed into the Home page.
Inside Home page there are 3 elements,
  - Hello message component.
  - Logout button.
  - Draggable user thumbnail.
After clicking the logout button Home component saves the last icon position in DB.

### How do I get set up? ###

- Clone repository using git clone https://bitbucket.org/pinikeizman/rapidapi.git.
- Run the npm install  / yarn install command.
- Hit npm start to lunch the application on localhost:3000.
- There is only one user hard coded in the application:

{
  	"username": "user@rapidapi.com",
	  "password": 12345
}
